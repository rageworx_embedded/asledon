////////////////////////////////////////////////////////////////////////////////
// Anystreaming LED just on app.
// ---------------------------------------------------------------------------
// (C)2016,2016 Raphael Kim @ 3Iware
//
//  reversion 0.1
//
////////////////////////////////////////////////////////////////////////////////


#include <unistd.h>
#include <signal.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>


#include <fcntl.h>
#include <sys/ioctl.h>
// Anystreaming LED IO definition
#include "gpio_export.h"

#define GPIO_LED_RED        "/dev/gpio5"
#define GPIO_LED_GREEN      "/dev/gpio12"
#define GPIO_LED_BLUE       "/dev/gpio13"

int fd_led_red = -1;
int fd_led_green = -1;
int fd_led_blue = -1;

////////////////////////////////////////////////////////////////////////////////

bool open_led_devs()
{
    fd_led_blue = open( GPIO_LED_BLUE, O_RDWR );
    if ( fd_led_blue >= 0 )
    {
        ioctl( fd_led_blue, GPIO_PINDIR_OUT );
    }

    fd_led_green = open( GPIO_LED_GREEN, O_RDWR );
    if ( fd_led_green >= 0 )
    {
        ioctl( fd_led_green, GPIO_PINDIR_OUT );
    }

    fd_led_red = open( GPIO_LED_RED, O_RDWR );
    if ( fd_led_red >= 0 )
    {
        ioctl( fd_led_red, GPIO_PINDIR_OUT );
    }

#ifdef DEBUG
    printf( "fd_led_red = %d\n", fd_led_red );
    printf( "fd_led_green = %d\n", fd_led_green );
    printf( "fd_led_blue = %d\n", fd_led_blue );
#endif // DEBUG

    if ( ( fd_led_red >= 0 ) && ( fd_led_green >= 0 ) && ( fd_led_blue >= 0 ) )
    {
        return true;
    }

    return false;
}

void ctrl_set_led( bool red, bool green, bool blue )
{
    if ( red == true )
    {
        if ( fd_led_red >= 0 )
        {
            ioctl( fd_led_red, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_red >= 0 )
        {
            ioctl( fd_led_red, GPIO_DATA_SET );
        }
    }

    if ( green == true )
    {
        if ( fd_led_green >= 0 )
        {
            ioctl( fd_led_green, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_green >= 0 )
        {
            ioctl( fd_led_green, GPIO_DATA_SET );
        }
    }

    if ( blue == true )
    {
        if ( fd_led_blue >= 0 )
        {
            ioctl( fd_led_blue, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_blue >= 0 )
        {
            ioctl( fd_led_blue, GPIO_DATA_SET );
        }
    }
}

void close_led_devs()
{
    ctrl_set_led( false, false, false );

    if ( fd_led_red >= 0 )
    {
        close( fd_led_red );
        fd_led_red = -1;
    }

    if ( fd_led_green >= 0 )
    {
        close( fd_led_green );
        fd_led_green = -1;
    }

    if ( fd_led_blue >= 0 )
    {
        close( fd_led_blue );
        fd_led_blue = -1;
    }
}

bool sigint_mutexed = false;

void sigint_handler( int signo)
{
    //signal( SIGINT, NULL );
    if ( sigint_mutexed == false )
    {
        sigint_mutexed = true;

        close_led_devs();

        exit( 0 );
    }
}

int main (int argc, char ** argv)
{
    printf("Anystream LED on : (C)2016 3Iware, Rah.K / rage.kim@3iware.com\n");

    signal( SIGINT, sigint_handler );

    if ( open_led_devs() == false )
    {
        printf("ERROR: LED config failure !\n");
    }

    for( int cnt=0;cnt<20;cnt++)
    {
        if ( cnt%2 == 0 )
        {
            ctrl_set_led( false, true, false );
        }
        else
        {
            ctrl_set_led( false, false, false );
        }

        sleep( 1 );
    }

    close_led_devs();

    return 0;
}
