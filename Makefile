# Makefile for asctrld,
# scripted by Raph.K. (rage.kim@3iware.com)

# Notice :
# You may need check 'env' to exists TOOLSDIR.

#CCPATH = /opt/Mozart_toolchain/arm-eabi-uclibc/usr
#CCBIN  = ${CCPATH}/bin
CCBIN    = ${TOOLSDIR}
CCPREFIX = arm-linux-

GCC = ${CCBIN}/${CCPREFIX}gcc
GPP = ${CCBIN}/${CCPREFIX}g++
AR  = ${CCBIN}/${CCPREFIX}ar

SOURCEDIR  = ./src
OBJDIR     = ./obj/Release
OUTBINNAME = asledon
OUTPUTDIR = ./bin/Release
DEFINEOPT = -D_GNU_SOURCE
OPTIMIZEOPT = -O2 -fexpensive-optimizations

CFLAGS := -I$(SOURCEDIR) -D$(DEFINEOPT) $(OPTMIZEOPT)

all: ${OUTPUTDIR}/${OUTBINNAME}

${OUTPUTDIR}/${OUTBINNAME}:
	$(GPP) ${SOURCEDIR}/main.cpp -lpthread -lm -o $@

install:
	@./cp2nfs.sh --release

clean:
	@-rm -f ${OBJDIR}/*.o ${OUTPUTDIR}/${OUTBINNAME}
