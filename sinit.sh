#!/bin/bash

BIN_PATH="./bin"
BIN_DEBUG_PATH="${BIN_PATH}/Debug"
BIN_RELEASE_PATH="${BIN_PATH}/Release"

OBJ_PATH="./obj"
OBJ_DEBUG_PATH="${OBJ_PATH}/Debug"
OBJ_RELEASE_PATH="${OBJ_PATH}/Release"

CHK_PATHES="$BIN_PATH $BIN_DEBUG_PATH $BIN_RELEASE_PATH $OBJ_PATH $OBJ_DEBUG_PATH $OBJ_RELEASE_PATH"

for rpt in $CHK_PATHES
do
	if [ ! -e $rpt ]; then
		mkdir $rpt
	fi
done
